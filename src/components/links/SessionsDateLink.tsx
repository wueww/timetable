import React, { ReactNode } from "react";
import { Link } from "react-router-dom";
import { linkToSessionsDate } from "../../util/links";

export default function SessionsDateLink({ date, children }: { date: string; children: ReactNode }) {
    return <Link to={linkToSessionsDate(date)}>{children}</Link>;
}
