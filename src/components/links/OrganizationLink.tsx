import React, { ReactNode } from "react";
import { Link } from "react-router-dom";
import { linkToHost } from "../../util/links";

export default function OrganizationLink({ id, children }: { id: number; children: ReactNode }) {
    return <Link to={linkToHost(id)}>{children}</Link>;
}
