import { useSelector } from "react-redux";
import { RootState } from "../store";

export const useChannels = () => {
    return useSelector((state: RootState) => state.sessions.channels || []);
};
