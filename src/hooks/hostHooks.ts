import { useSelector } from "react-redux";
import { RootState } from "../store";

export const useHost = (id: number) => {
    const sessions = useSelector((state: RootState) => state.sessions.sessions);
    const candidate = sessions.find(({ host }) => id === host.id);
    return candidate ? candidate.host : undefined;
};
