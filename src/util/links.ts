export const linkToSessionsDate = (date: string) => `/veranstaltungen-am-${date}`;

export const PATH_TO_EVENT = "/veranstaltung/:id";
export const linkToSession = (id: number) => `/veranstaltung/${id}`;

export const PATH_TO_ORGANIZER = "/veranstalter/:id";
export const linkToHost = (id: number) => `/veranstalter/${id}`;
