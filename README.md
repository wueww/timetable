# Wuerzburg Web Week Timetable

## Getting started

Die Anwendung basiert (inzwischen) auf `react-scripts` a.k.a. CRA.

```
npm run install
npm run start
```

... dann sollte unter http://localhost:3000/ der Timetable auftauchen. Browser sollte sich
automatisch öffnen.

## Modes of operation

### Normal

Der Timetable wird regulär gerendert, das heißt mit Datums- & Kategorienfilter und Darstellung
sämtlicher Sessions.

### Highlight Only

Nur die im Backend als `highlight` markierten Sessions werden gelistet. Dabei werden die
Sessions über alle Tages hinweg, jedoch gegliedert, untereinander weg gelistet. Der Datums-
und Kategorienfilter wird nicht dargestellt.

Zum Aktivieren muss am `#app`-Knoten im HTML noch `data-mode="highlight-only"` erfasst werden.

### Embedder's Choice

In diesem Modus kann der Einbettende bestimmen, welche Sessions gelistet werden sollen.
Die Darstellung erfolgt wie bei _Highlight Only_ auch.

Zum Aktivieren müssen am `#app`-Knoten zusätzlich die Attribute
`data-mode="embedder-choice" data-session-ids="564,579,572"` geliefert werden.
